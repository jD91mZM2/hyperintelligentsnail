# Hyper Intelligent Snail

*Name is an /r/AskReddit inside joke*

This is a crazy idea where I'm experimenting with improved pathfinding for
Minecraft. It's currently built on A* with a simple heuristic based on the
manhattan-distance.

Because it's not that fast, I am thinking about making a special kind of zombie
that instructs other zombies on where to enter a base or such. This could be a
colonel zombie. To make it even cooler, there could be special kinds of
creepers and zombies that walk this path and potentially explode blocks that
are in the way of the cheapest path. That would be fairly cool.

Another crazy idea is if zombies can randomly build nether portals where some
nether mobs like ghasts "come out of" to help destroy a base and chase down a
player. Something could also prevent a player from entering one, if that'd be
too OP of an advantage.
