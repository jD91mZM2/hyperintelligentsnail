package one.krake.hyperintelligentsnail

import com.mojang.brigadier.Command
import com.mojang.brigadier.CommandDispatcher
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType
import net.minecraft.block.Blocks
import net.minecraft.command.arguments.BlockPosArgumentType
import net.minecraft.server.command.CommandManager
import net.minecraft.server.command.ServerCommandSource
import net.minecraft.text.TranslatableText
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec3i
import one.krake.hyperintelligentsnail.path.PathFinder

object PathfindCommand {
    val ERROR = SimpleCommandExceptionType(TranslatableText("cmd.pathfind.error", PathFinder.MAX_SEARCH_DEPTH))

    fun register(dispatcher: CommandDispatcher<ServerCommandSource>) {
        dispatcher.register(
            CommandManager.literal("pathfind")
                .then(
                    CommandManager.argument("target", BlockPosArgumentType.blockPos())
                        .executes { ctx -> try {
                            val target = BlockPosArgumentType.getBlockPos(ctx, "target");
                            val source = ctx.source.position;
                            val path = PathFinder(
                                ctx.source.world,
                                Vec3i(source.x, source.y, source.z),
                                target
                            ).findPath()

                            if (path == null) {
                                throw ERROR.create()
                            } else {
                                for (block in path) {
                                    ctx.source.world.setBlockState(
                                        BlockPos(block).down(),
                                        Blocks.OAK_PLANKS.defaultState
                                    );
                                }
                            }

                            Command.SINGLE_SUCCESS
                        } catch (err: Exception) {
                            err.printStackTrace()
                            throw err;
                        }}))
    }
}