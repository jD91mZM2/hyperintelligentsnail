package one.krake.hyperintelligentsnail.path

import net.minecraft.block.Blocks
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Vec3i
import net.minecraft.world.World
import java.util.*
import kotlin.Comparator

class PathFinder(val mcWorld: World, val start: Vec3i, val end: Vec3i) {
    companion object {
        val MAX_SEARCH_DEPTH = 100_000
    }

    val world = PathWorld(this.mcWorld)
    val first = this.world.getOrDefault(start, null)
    val last = this.world.getOrDefault(end, null)
    init {
        this.first.distance = 0
    }

    val queue = PriorityQueue<PathNode>(Comparator<PathNode> { node1, node2 ->
        node1.estimateTo(this.end) - node2.estimateTo(this.end)
    })
    val completed = HashSet<PathNode>()
    init {
        this.queue.add(this.first)
    }

    fun findPath(): List<Vec3i>? {
        for (i in 0..PathFinder.MAX_SEARCH_DEPTH) {
            // You know what, there's no reason to even check if the queue is empty.
            // It won't be. The world is just so big.
            val node = this.queue.remove()
            this.completed.add(node)
            this.mcWorld.setBlockState(
                BlockPos(node.pos).down(),
                Blocks.GREEN_WOOL.defaultState
            );

            println("#$i (${node.pos.x}, ${node.pos.y}, ${node.pos.z}) (${node.distance}m)")

            if (node == this.last) {
                return node.reconstructPath()
            }

            for (connected in node.adjacent) {
                connected.update(node)
                if (!this.completed.contains(connected)) {
                    this.queue.add(connected)
                    this.mcWorld.setBlockState(
                        BlockPos(connected.pos).down(),
                        Blocks.RED_WOOL.defaultState
                    );
                }
            }
        }
        return null
    }
}
