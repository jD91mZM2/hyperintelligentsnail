package one.krake.hyperintelligentsnail.path

import com.google.common.math.IntMath
import net.minecraft.entity.ai.pathing.Path
import net.minecraft.util.math.BlockPos
import net.minecraft.util.math.Direction
import net.minecraft.util.math.Vec3i
import net.minecraft.world.World
import java.lang.Math.pow
import java.math.RoundingMode
import java.util.*
import kotlin.math.abs
import kotlin.math.pow

class PathNode(
    val world: PathWorld,
    var predecessor: PathNode?,
    val pos: Vec3i
) {
    val adjacent: List<PathNode>
        get() = listOf(Pair(0, -1), Pair(0, 1), Pair(-1, 0), Pair(1, 0)).flatMap { dir ->
            (-1..1).map { y ->
                this.world.getOrDefault(
                    Vec3i(
                        this.pos.x + dir.first,
                        this.pos.y + y,
                        this.pos.z + dir.second
                    ),
                    this
                )
            }
        }.filter { node -> node.isValid() }

    var distance: Int = Int.MAX_VALUE;

    fun isValid(): Boolean {
        val pos = BlockPos(this.pos);
        val world = this.world.mcWorld;
        return !world.getBlockState(pos).material.isSolid
                && !world.getBlockState(pos.up()).material.isSolid
                && world.getBlockState(pos.down()).material.isSolid
    }

    /**
     * Returns the sum of the distance travelled and the manhattan distance estimated to be remaining
     */
    fun estimateTo(pos: Vec3i): Int {
        // The g(n) part of A*
        var g = this.distance;

        // The h(n) part of A* - manhattan distance
        var h = (abs(pos.x - this.pos.x)
                + abs(pos.y - this.pos.y)
                + abs(pos.z - this.pos.z))

        // Scale it 1000 times
        // Weigh h(n) by 1.5 to make it suboptimal but faster
        g *= 1000;
        h *= 10000;

        return g + h
    }

    fun update(predecessor: PathNode) {
        // Don't update the very first node
        if (this.distance == 0) {
            return
        }

        // Update the distance
        if (this.predecessor != null) {
            this.distance = this.predecessor!!.distance + 1;
        }

        // Update predecessor if we found a shorter path
        val newDistance = predecessor.distance + 1;
        if (newDistance < this.distance) {
            this.predecessor = predecessor;
            this.distance = newDistance;
        }
    }

    fun reconstructPath(): List<Vec3i> {
        val path = arrayListOf(this.pos)
        var current = this;

        while (current.predecessor != null) {
            current = current.predecessor!!;
            path.add(current.pos);
        }

        return path.reversed();
    }

    override fun hashCode(): Int {
        return this.pos.hashCode()
    }
    override fun equals(other: Any?): Boolean {
        return if (other != null && other is PathNode) {
            this.pos == other.pos
        } else {
            super.equals(other)
        }
    }
}
