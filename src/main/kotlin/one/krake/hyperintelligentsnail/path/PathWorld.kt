package one.krake.hyperintelligentsnail.path

import net.minecraft.util.math.Vec3i
import net.minecraft.world.World

class PathWorld(val mcWorld: World) {
    val nodes = HashMap<Vec3i, PathNode>()

    fun getOrDefault(pos: Vec3i, predecessor: PathNode?): PathNode {
        return this.nodes.getOrPut(pos) {
            PathNode(this, predecessor, pos)
        }
    }
}