package one.krake.hyperintelligentsnail.mixin;

import net.minecraft.client.gui.screen.TitleScreen;
import net.minecraft.entity.ai.TargetFinder;
import net.minecraft.entity.ai.pathing.*;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.mob.MobEntityWithAi;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.chunk.ChunkCache;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.*;
import java.util.function.ToDoubleFunction;

@Mixin(PathNodeNavigator.class)
// Mixins HAVE to be written in java due to constraints in the mixin system.
public class PathfindMixin {
    @Overwrite
    private Path findPathToAny(PathNode startNode, Map<TargetPathNode, BlockPos> positions, float followRange, int distance, float rangeMultiplier) {
        BlockPos target = positions.values().iterator().next();
        System.out.println("TODO");
        return new Path(
                Arrays.asList(new PathNode(0, 0, 0)),
                target,
                true
        );
    }
}
